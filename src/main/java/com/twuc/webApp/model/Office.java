package com.twuc.webApp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Office {
    @Id
    private int id;
    @Column
    private String city;

    public Office(int id, String city) {
        this.id = id;
        this.city = city;
    }

    public Office() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }
}

