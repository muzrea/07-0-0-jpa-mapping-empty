package com.twuc.webApp;

import com.twuc.webApp.model.Office;
import com.twuc.webApp.model.OfficeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class OfficeTest {
    @Autowired
    private OfficeRepository repo;

    @Autowired
    private EntityManager em;

    @Test
    void should_return_true() {
        assertTrue(true);
    }

    @Test
    void should_save_new_office() {
        Office saveOffice = repo.save(new Office(1,"Xi'an"));
        em.flush();
        assertNotNull(saveOffice);
    }

    @Test
    void should_save_office_use_em() {
        em.persist(new Office(1,"Wuhan"));
        em.flush();
        em.clear();
        Office office = em.find(Office.class, 1);
        assertEquals(1,office.getId());
        assertEquals("Wuhan",office.getCity());
    }
}
